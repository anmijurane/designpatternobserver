package PatronObservador;

public interface SujetoObservado {

    public void notificar(); //notificar cuando hay un evento

}

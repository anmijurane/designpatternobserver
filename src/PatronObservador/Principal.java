package PatronObservador;

import java.util.Scanner;

public class Principal {
    static Scanner sr = new Scanner(System.in);
    static int Select = 5;
    static boolean MotorON = false;

    public static void main(String[] args) {
        menu();
    }

    public static void menu() {

        while (Select != 0) {
            System.out.println("**********MENU**********");
            System.out.println("1.- Prender Motor");
            System.out.println("2.- Abrir los vidrios");
            System.out.println("3.- Acelerar");
            System.out.println("4.- Apagar Coche");
            System.out.println("0.- Salir");
            System.out.println("_____________________");
            System.out.print("[  Elige una opción: ");
            Select = sr.nextInt();
            System.out.println("**********/*********");

            if (Select == 1) {
                MotorON = true;
                System.out.println("-----");
                EncenderMotor mtr = new EncenderMotor();
                AccObserver obj = new AccObserver();
                obj.EnlazarObservador(mtr);
                obj.EncenderCoche();

            } else if (Select == 2 & MotorON) {

                System.out.println("-----");
                Vidrios vdr = new Vidrios();
                AccObserver obj = new AccObserver();
                obj.EnlazarObservador(vdr);
                obj.AbrirVidrios();

            } else if (Select == 3 & MotorON) {

                System.out.println("-----");
                Acelerar acl = new Acelerar();
                AccObserver obj = new AccObserver();
                obj.EnlazarObservador(acl);
                obj.PisarAcelerador();

            } else if (Select == 4 & MotorON) {

                System.out.println("-----");
                MotorON = false;
                ApagarMotor ApgMtr = new ApagarMotor();
                AccObserver obj = new AccObserver();
                obj.EnlazarObservador(ApgMtr);
                obj.ApagarCoche();

            }else if (Select == 0) {
                System.out.println("ADIOS");
                System.exit(0);
            }else{
                System.out.println("Enciende el motor primero");
            }
            System.out.println("______________________________");
        }
    }
}

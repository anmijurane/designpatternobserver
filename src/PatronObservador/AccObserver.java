package PatronObservador;
//Acciones de obserar
import java.util.ArrayList;

public class AccObserver implements SujetoObservado{
    private final ArrayList<Observer> Observadores;

    //Constructor
    public AccObserver() {
        Observadores = new ArrayList<Observer>();
    }

    //METODOS DE NOTIFICACIÓN
    void EncenderCoche() { notificar(); }

    void AbrirVidrios() { notificar(); }

    void PisarAcelerador() { notificar(); }

    void ApagarCoche(){ notificar(); }

    //IMPLEMENTACION DEL LA INTERFAZ
    public void EnlazarObservador(Observer o) {
        Observadores.add(o);
    }

    @Override
    public void notificar() {
        for (Observer o : Observadores) {
            o.Actualizar();
        }
    }
}

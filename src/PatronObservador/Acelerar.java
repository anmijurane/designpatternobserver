package PatronObservador;

public class Acelerar implements Observer{

    @Override
    public void Actualizar() {

        System.out.println("Acelerando... ");
        for (int i=0; i <= 100; i++){
            i = i +9;
            System.out.println(i+" Km/h");
        }
    }

    public Acelerar(){    }
}
